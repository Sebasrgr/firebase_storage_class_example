import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage_app/model/apunte.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  List<Apunte> _apuntesList;
  List<DocumentSnapshot> _documentList;
  List<Apunte> get getApuntesList => _apuntesList; // kesesto

  HomeBloc() : super(HomeInitial());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is GetDataEvent) {
      bool dataRetrieved = await _getAllApuntes();
      if (dataRetrieved)
        yield CloudStoreGetData();
      else
        yield CloudStoreError(errorMessage: 'Error al cargar los datos');
    } else if (event is SaveDataEvent) {
      bool saved = await _saveApunte(
        event.materia,
        event.descripcion,
        event.imageUrl,
      );
      if (saved) {
        await _getAllApuntes();
        yield CloudStoreGetData();
      } else {
        yield CloudStoreError(
            errorMessage: 'Ha ocurrido un error AAAaaaAAAaaAAaAA');
      }
    } else if (event is UploadFileEvent) {
      File chosenImage = await _pickImage();
      if (chosenImage != null) {}
    }
  }

  Future<File> _pickImage() async {
    final picker = ImagePicker();

    final PickedFile chosenImage = await picker.getImage(
      source: ImageSource.camera,
      maxHeight: 720,
      maxWidth: 720,
      imageQuality: 85,
    );
    return File(chosenImage.path);
  }

  Future<String> _uploadPicture(File image) async {
    String imagePath = image.path;

    //firebase storage reference
    StorageReference reference = FirebaseStorage.instance
        .ref()
        .child('apuntes/${Path.basename(imagePath)}');

    StorageUploadTask uploadTask = reference.putFile(image);
    //StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    await uploadTask.onComplete;

    // these lines do exactly the same thing.
    // dynamic imageUrl = taskSnapshot.ref.getDownloadURL();
    dynamic imageURL = await reference.getDownloadURL();
    return imageURL;
  }

  Future<bool> _saveApunte(
    String materia,
    String descrpicion,
    String imageUrl,
  ) async {
    try {
      await FirebaseFirestore.instance.collection('apuntes').doc().set({
        "materia": materia,
        "descripcion": descrpicion,
        "imagen": imageUrl,
      });
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> _getAllApuntes() async {
    try {
      var apuntes =
          await FirebaseFirestore.instance.collection('apuntes').get();
      _apuntesList = apuntes.docs
          .map((e) => Apunte(
                materia: e['materia'],
                descripcion: e['descripcion'],
                imageUrl: e['imagen'],
              ))
          .toList();
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }
}
